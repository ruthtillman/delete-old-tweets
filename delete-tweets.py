#!/usr/bin/env python

import sys
import time
import os
import twitter
import io

__author__ = "Koen Rouwhorst, adaped Ruth Tillman"
__version__ = "0.1.1"

def delete(api):
    with io.open("delete_keys.txt", encoding='utf-8') as file:
        count = 0
        for row in file:
            tweet_id = int(row.replace(' ',''))
            try:
                print("Deleting tweet #{0}".format(tweet_id))
                api.DestroyStatus(tweet_id)
                count += 1
                time.sleep(0.5)
            except twitter.TwitterError as err:
                print("Exception: %s\n" % err.message)
    print("Number of deleted tweets: %s\n" % count)

def error(msg, exit_code=1):
    sys.stderr.write("Error: %s\n" % msg)
    exit(exit_code)

def main():
    api = twitter.Api(consumer_key='', # if these are not set, the script will not run!
                      consumer_secret='',
                      access_token_key='',
                      access_token_secret='')
    delete(api)

if __name__ == "__main__":
    main()
