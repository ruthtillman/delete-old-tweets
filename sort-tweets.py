import json

def read_JSON_file():
  with open("tweet.js", 'r') as record:
  	tweets = json.load(record)
  	return tweets

def get_id(json):
	try:
		return int(json['tweet']['id'])
	except KeyError:
		return 0

def sort_JSON_file():
	presort = read_JSON_file()
	presort.sort(key=get_id, reverse=True)
	return presort

stuff = sort_JSON_file()

def write_JSON_file(content,newJson):
  with open(newJson, 'w') as outfile:
    json.dump(content, outfile, sort_keys=True, indent=4)

write_JSON_file(stuff,"tweet.js")
