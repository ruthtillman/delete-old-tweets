# Delete tweets

Delete tweets (or just replies or retweets) from your timeline, including tweets
beyond the [3,200 tweet limit](https://web.archive.org/web/20131019125213/https://dev.twitter.com/discussions/276).

## Prerequisites

### Configure API access

1. You'll need an approved developer account.
1. Open Twitter's [Application Management](https://apps.twitter.com/), and create
  a new Twitter app.
1. Set the permissions of your app to *Read and Write*.
1. Set the variables in `delete-tweets.py`. Or revise them as environmental variables. Don't commit them to a public repo. You may just save them on your comp and add delete-tweets.py to a .gitignore.

```python
consumer_key="[your consumer key]"
consumer_secret="[your consumer secret]"
access_token_key="[your access token]"
access_token_secret="[your access token secret]"
```

### Get your tweet archive

1. Open your [Twitter account page](https://twitter.com/settings/account).
1. Scroll to the bottom of the page, click 'Request your archive', and wait for the email to arrive.
1. Follow the link in the email to download your Tweet archive.
1. Unpack the archive, find and move `tweets.js` to the same directory as this script.
1. Retain the original archive as a backup for your own records.

## Installation

Install the required dependencies.

```
pip install -r requirements.txt
```

## Usage

This set of scripts adapts the backbone of a CSV process created by Koen Rouwhorst.

It handles the new JSON export format, though you will need to remove a small amount of text from the file. It also provides a way of easily reading the tweets in the terminal and deciding whether or not to delete them (with failsafes in place before the actual deletion). You can also simply put the tweet IDs in delete_keys.txt and run the deletion script.

No warranty ETC. Use at your own risk. This is janky. Read all the docs below before starting (perhaps while waiting for your Twitter archive!)

Everything assumes you're running against `tweet.js` so retain a BACKUP of that file.

### 0: Prepare your tweet.js file

1. Open `tweet.js` in a text editor or terminal editor such as nano. Remove `window.YTD.tweet.part0 = ` from the first line of the file. Retain the opening `[`.
1. Run `sort-tweets.py` to sort tweet.js in chronological order from newest to oldest based on IDs. **Note:** this _will_ replace the original `tweet.js`. Edit `write_JSON_file(stuff,"tweet.js")` to change the output name or be prepared to replace from backup just in case. While the script doesn't require chronological order, this will help you make better decisions when reviewing tweets.
1. To view the outcome, run `tail -n 75 tweet.js` in the terminal to see the oldest tweet. If it looks right, continue.

### 1: Using parse-tweets.py

tl;dr use `parse-tweets.py` to review tweets and uses the following terminal responses:

* `y`: write the tweet's ID into delete_keys.txt
* `n`: ignore/retain tweet.
* `end`: stop the script and record your stopping place.

longer:

`parse-tweets.py` reads `tweet.js` into a JSON list. It reads forward from the END of the list (starting with the oldest tweet). It first prompts the user whether they would like to input a different starting place. Simply press "Enter" to go to the end of the list.

After a starting place has been chosen, the script displays the full_text field for a tweet. Choosing `y` will append the tweet's ID into `delete_keys.txt` which will be used in `delete-tweets.py`. Choosing `n` or pressing enter or entering any other key will `pass`.

You will then see the next tweet.

To stop working and either run deletions or come back later, type `end`. This saves your current place in the array to `stopping_place.txt`, which can be used when restarting this script and IS used if you choose to run `del-from-working-file.py`.

If you accidentally added a tweet to `delete_keys.txt`, it isn't yet running the deletes so you can open the file, find the ID, and remove it. Be sure not to add extra line breaks as this will cause a headache when running `delete-tweets.py`.

**Advanced:**

If you missed a tweet: typing `v#`, where # stands for an integer, will return you X # of tweets prior to the current tweet you're judging and allow you to re-review it. IF YOU ALREADY PRESSED Y, pressing `n` will NOT remove it from delete_keys.txt. However, if you chose `n` or otherwise passed on it, this will allow you to choose to delete instead. You may also choose `n` on it a second time, which is particularly important in cases where you misjudged the number to enter.

The script will then show you the tweet you had just seen but not judged before returning you to the normal array. You cannot use `end` until you have judged the tweet at position v# and the most recent tweet. Recommend waiting on using this until you've gotten familiar with the rest.

### 2: Using delete-tweets.py

tl;dr `delete-tweets.py` reads IDs from `delete_keys.txt` and sends deletes to Twitter's API using your auth credentials. To avoid throttling, it sleeps 0.5 seconds between deletions.

`delete-tweets.py` does the actual deletions. It self-validates with the API, so your keys need to be in there.

It opens `delete_keys.txt` and reads it line by line, casting the lines to integer, then sends them as part of an API delete command. It will return a message that the tweet with that ID was deleted OR it'll return the appropriate message. If a user you RTd has been suspended or deleted their tweet, you'll get an error reflecting that.

It does not currently handle blank lines in the middle of the file, so it will throw and error and stop. Removing all the (already-processed) keys up to that point and running the script again will solve that.

Once you successfully run this script, consider deleting `delete_keys.py`, so you won't end up trying the same deletion twice, which yields an error.

### 3: Using stopping_place.txt and del-from-working-file.py

Whenever you end `parse-tweets.py`, the file `stopping_place.txt` is rewritten with your _current_ point in the _current_ version of `tweet.js` (or, really, the point for the most recent tweet you judged using `y` or `n`, not the one you were looking at when you typed `end`, because it still needs a judgment.)

This allows you to skip over previously reviewed tweets in one of two ways:

**Skipping in the array:** when starting `parse-tweets.py`, it will ask you if you want to start at a place other than the very end of the array. If you are not managing your working file with `del-from-working-file.py`, you'll want to enter the number from `stopping_place.txt`. This will take you to the tweet you were reviewing when you typed `end`.

**Manage your working file:** `del-from-working-file.py` will allow you to manage your working file. It gets the overall length of tweet.js, then determines the number of tweets you reviewed, based on the position stored in stopping place. NOTE, this number will begin with `-`, e.g. `-241`. It will display this number and ask to what point you want to delete.

Be sure to enter the whole number, such as `-230`. The script allows you to cut ANY number of tweets off the end of your working file and will resave it as tweet.js.

The good news is... if you screw up and enter `2300` when you meant `230`? Stopping place records your stopping place from the beginning of the file, but deletes run from the end of the file. You can replace tweet.js with your original download and rerun `del-from-working-file.py` to get the total number of tweets you've reviewed up to this point.
