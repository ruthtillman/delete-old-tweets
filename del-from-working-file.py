import json, sys, os.path

def read_JSON_file():
  with open("tweet.js", 'r') as record:
  	tweets = json.load(record)
  	items = len(tweets)
  	return tweets, items

def write_JSON_file(content,newJson):
  with open(newJson, 'w') as outfile:
    json.dump(content, outfile, sort_keys=True, indent=4)

def delete_JSON(length,tweets):
	tweets = tweets[:length]
	write_JSON_file(tweets,"tweet.js")

def iterate_JSON():
	thing = read_JSON_file()
	tweets = thing[0]
	length = thing[1]
	if os.path.exists("stopping_place.txt"):
		with open("stopping_place.txt","r") as stop:
			the_thing = int(stop.readline())
			length = int(length)
			stop = the_thing - length
			stopping_place = "Previous stopping place: " + str(stop)
			print(stopping_place)
	else:
		pass
	resume = input("To delete tweets, enter the place to delete: ")
	if resume == "":
		pass
	else:
		length = int(resume)
		delete_JSON(length,tweets)

iterate_JSON()
