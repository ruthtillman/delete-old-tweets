import json, sys, os.path

# Documenting Keys #
# dict_keys(['retweeted', 'source', 'entities', 'display_text_range', 'favorite_count', 'in_reply_to_status_id_str', 'id_str', 'in_reply_to_user_id', 'truncated', 'retweet_count', 'id', 'in_reply_to_status_id', 'possibly_sensitive', 'created_at', 'favorited', 'full_text', 'lang', 'in_reply_to_screen_name', 'in_reply_to_user_id_str', 'extended_entities'])
# dict_keys(['retweeted', 'source', 'entities', 'display_text_range', 'favorite_count', 'in_reply_to_status_id_str', 'id_str', 'in_reply_to_user_id', 'truncated', 'retweet_count', 'id', 'in_reply_to_status_id', 'created_at', 'favorited', 'full_text', 'lang', 'in_reply_to_screen_name', 'in_reply_to_user_id_str'])

# Steps
# input enter to print each tweet
# input Y to write (append) the tweet ID to the end of a file and display then next tweet
# input N not to.
# advanced: input V# to re-view the entry current - # tweets. And then Y would remove it from the end of the file and if not ....

def read_JSON_file():
  with open("tweet.js", 'r') as record:
  	tweets = json.load(record)
  	items = len(tweets)
  	return tweets, items

def do_output(num,tweets):
	with open("delete_keys.txt", "a") as output:
		output.write(tweets[num]['tweet']['id'])
		output.write("\n")

def do_endplace(num,tweets):
	with open("stopping_place.txt", "w") as output:
		num = num + 1
		output.write(str(num))
	sys.exit()

def do_review(num,command,tweets):
	offset = int(command[1])
	review = num + offset
	review_tweet="REVIEW: " + tweets[review]['tweet']['full_text']
	print(review_tweet)
	verdict = input("Delete or keep? ")
	if verdict.lower() == "y":
		do_output(review,tweets)
	elif verdict.lower() == "n":
		pass
	else:
		pass
	follow_up_review(num,tweets)

def follow_up_review(num,tweets):
	print("Current tweet:")
	print(tweets[num]['tweet']['full_text'])
	verdict_current = input("Delete or keep? ")
	if verdict_current.lower() == "y":
		do_output(num,tweets)
	elif verdict_current.lower() == "n":
		pass # What this actually needs is me to dig into how to get the tweet ID and remove it from the delete_tweets file.
	else:
		pass

def get_command(num,tweets):
	command = input("")
	if command == "":
		pass
	elif command[0].lower() == "v":
		do_review(num,command,tweets)
	elif command.lower() == "y":
		do_output(num,tweets)
	elif command.lower() == "n":
		pass
	elif command.lower() == "end":
		do_endplace(num,tweets)

def iterate(length,tweets):
	num = length - 1
	while num <= length:
		print(tweets[num]['tweet']['full_text'])
		get_command(num,tweets)
		num = num - 1

def iterate_JSON():
	thing = read_JSON_file()
	tweets = thing[0]
	length = thing[1]
#	if os.path.exists("stopping_place.txt"):
#		with open("stopping_place.txt","r") as stop:
#			stopping_place = "Previous stopping place: " + stop.readline()
#			print(stopping_place)
#	else:
#		pass
	resume = input("To resume at another point, enter the previous data point: ")
	if resume == "":
		iterate(length,tweets)
	else:
		length = int(resume)
		iterate(length,tweets)
		

	
iterate_JSON()
